/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Hole_Wall_h
#define _Hole_Wall_h 1

#include "shared/src/net/http/server/http_server.h"
#include "shared/src/net/http/server/http_server_req_handler.h"
#include "shared/src/net/hole/hole_node.h"
#include "shared/src/net/hole/hole_con.h"
#include "shared/src/net/hole/hole_con_pool.h"

#include "boost/asio.hpp"

class HoleWall;

class ReqH : public HttpServerReqHandler, public Lockable
{
public:
	ReqH( HoleWall &_wall )
	: wall(_wall) {};
	~ReqH() {};

	bool Process( HttpInRequest &req );
protected:
	HoleWall	&wall;

	char	outBuf[ 100000 ];
};

class HoleWall : public Lockable, public HoleNode
{
	friend class ReqH;
public:
	HoleWall(	boost::asio::io_service &ioService,
					boost::asio::ip::udp::endpoint _outUdpEp,
					unsigned short _httpPort,
					unsigned short _minUdpPort,
					unsigned short _maxUdpPort
				);
	~HoleWall();

	bool Start();

protected:
	void 					doChores();
	std::string			newHole();
	unsigned short		generatePortNumber();
	std::string			generatePath();

protected:
	HttpServer					server;
	boost::asio::io_service			&ioService;
	HoleConPool					pool;
	boost::asio::ip::udp::endpoint	outUdpEp;
	ReqH							reqHandler;
	
	unsigned short	minUdpPort;
	unsigned short	maxUdpPort;
	unsigned short	lastUdpPort;
	
	int lastPathNum;
};

HoleWall::HoleWall(	boost::asio::io_service &_ioService,
							boost::asio::ip::udp::endpoint _outUdpEp,
							unsigned short _httpPort,
							unsigned short _minUdpPort,
							unsigned short _maxUdpPort
		) : 	HoleNode( _ioService ),
				ioService(_ioService),
				outUdpEp(_outUdpEp),
				server(_ioService, _httpPort, reqHandler ),
				reqHandler(*this),
				minUdpPort(_minUdpPort),
				maxUdpPort(_maxUdpPort),
				lastUdpPort(_minUdpPort),
				lastPathNum(0)
{};

HoleWall::~HoleWall() {};

bool HoleWall::Start()
{
	startTickTimer();
	server.Start();
	return server.IsListening();	
}

std::string HoleWall::newHole()
{

	std::string path = generatePath();
	unsigned short pn = 0;

	HoleCon *hc = new HoleCon( ioService, 0, path, false, outUdpEp );

	if(( hc == NULL )||  !pool.AddCon( hc ) ) {
		LOG_HOLE_ERROR("FAILED to create a new HoleCon!\n");
		if( hc != NULL ) { delete hc; }
		return "";
	}

	LOG_HOLE_INFO("NEW HOLE! port: %u, path: \"%s\".\n", hc->GetPort(), path.c_str() );

	hc->Start();

	return path;
}

std::string HoleWall::generatePath()
{
	std::string dst;
	++lastPathNum;
	unsigned int rnd = rand() & 0xffff;
	StrUtils::Printf( dst, "%d-%u", lastPathNum, rnd );
	return dst;
}

class ChoreDoer
{
public:
	ChoreDoer() {};
	int operator()( HoleCon *hc )
	{
		hc->DoChores();
		return AsyncPool<HoleCon>::ASP_CONTINUE;
	};
	static int CallBound( HoleCon *hc, void *obj ) {
		return ( *((ChoreDoer*)obj) )( hc );
	};
};

void HoleWall::doChores()
{
	LOCK_ME;	

	ChoreDoer chd;
	pool.ForEach( ChoreDoer::CallBound, &chd ); 	
}

// ------- class ReqH ---------

bool ReqH::Process( HttpInRequest &req )
{
	LOCK_ME;

	if( req.GetMethod() != METH_PUT ) {
		LOG_HOLE_ERROR("I only recognize put request, not %d.\n", req.GetMethod() );
		return false;
	}

	if( !req.GetUrl().Parse() ) {
		LOG_HOLE_ERROR("Weird url! \"%s\".\n", req.GetUrl().url.c_str() );
		return false;
	}
	std::string path = req.GetUrl().path;
	if( !path.empty() && path[0] == '/' ) {
		path.erase(0,1);
	}

	if( 0 == path.compare("open" ) ) {
		std::string newPath = wall.newHole();
		if( newPath.empty() ) {
			LOG_HOLE_ERROR("Failed to create a new hole.\n");
			return false;
		}
		req.RespondPlainText( newPath );
		return true;
	}

	LOG_HOLE_TRAFFIC("Looking up path: \"%s\".\n", path.c_str() );
	HoleCon *hc = wall.pool.FindByPath( path );
	if( hc == NULL ) {
		LOG_HOLE_ERROR("Path \"%s\" not found.\n", path.c_str() );
		return false;
	}

	hc->ReceiveHttp( req.GetInBody().c_str(), req.GetInBody().size() );
	size_t len = hc->SendHttp( outBuf, sizeof(outBuf) );
	req.RespondBinary( outBuf, len );
	return true;
}

#endif //_Hole_Wall_h
