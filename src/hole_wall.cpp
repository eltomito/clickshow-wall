/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "hole_wall.h"
#include "shared/src/net/asio_utils.h"

#include "boost/thread.hpp"

void printHelp()
{
	printf("Usage: test_hole_wall [<httpPort>] [<walled_udp_server_address>]\n");
}

int main( int argc, char* argv[] )
{
	boost::asio::io_service ioService;

	std::string walledServer = "127.0.0.1:9876";
	unsigned short minUdpPort = 10000;
	unsigned short maxUdpPort = 65535;
	unsigned short httpPort = 8080;

	if( argc > 1 ) {
		httpPort = atoi(argv[1]);
		if( httpPort == 0 ) {
			printf("Bad port number! %s\n", argv[1]);
			printHelp();
			exit(1);
		}
		if( argc > 2 ) {
			walledServer = argv[2];
		}
	}

	boost::asio::ip::udp::endpoint ep;
	if( !AsioUtils::StringToEndpoint( walledServer, ep, 9876 ) ) {
		printf("FAILED to create outgoing udp endpoint from \"%s\".\n", walledServer.c_str() );
		exit(1);
	};

	printf("Our target endpoint is \"%s\".\n", AsioUtils::EndpointToString( ep ).c_str() );

	HoleWall wall( ioService,
						ep,
						httpPort,
						minUdpPort, maxUdpPort );

	wall.Start();

	boost::thread *netThread = new boost::thread( boost::bind( &boost::asio::io_service::run, &ioService ) );
	if( netThread == NULL ) {
		printf("Thread creation failed.\n");
		exit(1);
	}

	printf( "Test wall started on port %u.\n", httpPort );

   char inBuf[ 255 ];
   ssize_t inlen;
   bool alive = true;
	while( alive ) {
		inlen = read( 0, inBuf, sizeof(inBuf) );
		if(inlen > 0) {
			if( ( inlen > 1 )&&( inBuf[0] == '/' ) ) {
				switch( inBuf[1] )
				{
					case 'q':
						printf("Bye!\n");
						alive = false;
					break;
				}
			} else {
				printf("Commands:\n/q - Quit.\n");
			}
		}
	}

	ioService.stop();
	delete netThread;
}
